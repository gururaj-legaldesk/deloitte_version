import {Injectable} from '@angular/core';
import {Http, Headers , Response} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class EsignService{
    constructor(private http:Http){
        console.log('Task Service Initialized...');
    }

    getTriggarOTP(aadhaar){

/*
       return  this.http.get('http://localhost:4000/api/esign/requestOTP/'+aadhaar).map((response:Response) => {

           console.log('response clarification',response)
           return response.json();
        }).subscribe();
*/
        return this.http.get('http://localhost:4000/api/esign/requestOTP/'+aadhaar)
            .map(res => res.json());
    }


    getSignedDocument(argument){

        return  this.http.get('http://localhost:4000/api/esign/getSigned/'+argument.aadhaar+'/'+argument.otp+'/'+argument.transactionId+'/'+argument.referenceId+'/'+argument.file)
            .map(res => res.json());
    }

    getTasks(){
        return this.http.get('/api/tasks')
            .map(res => res.json());
    }

    addTask(newTask){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/api/task', JSON.stringify(newTask), {headers: headers})
            .map(res => res.json());
    }

    deleteTask(id){
        return this.http.delete('/api/task/'+id)
            .map(res => res.json());
    }

    updateStatus(task){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('/api/task/'+task._id, JSON.stringify(task), {headers: headers})
            .map(res => res.json());
    }


}
