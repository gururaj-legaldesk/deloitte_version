/**
 * Created by gururaj on 4/17/17.
 */

var express       = require('express');
var path          = require('path');
var bodyParser    = require('body-parser');
var multer        = require('multer');


var index         = require('./routes/index');
var users         = require('./routes/users');
var esign         = require('./routes/esign');

var port          = 4000;

var app           = express();




//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

app.use(function(req, res, next) { //allow cross origin requests
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});


// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.use('/', index);

app.use('/api', users);
app.use('/api/users', users);

app.use('/api/esign', esign);
/*
app.get('/!*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/dist/index.html'));
});*/

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '_' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
});

var upload = multer({ //multer settings
    storage: storage
}).single('file');

/** API path that will upload the files */
app.post('/upload', function(req, res) {
    upload(req,res,function(err){
       // console.log(req.file);
        if(err){
            res.json({error_code:1,err_desc:err});
            return;
        }
        res.json({error_code:0,err_desc:null,fileInfo:req.file});
    });
});


/*app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)*/





app.listen(port, function(){
    console.log('Server started on port '+port);
});
/*

function logErrors (err, req, res, next) {
    console.error(err.stack)
    next(err)
}


function clientErrorHandler (err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({ error: 'Something failed!' })
    } else {
        next(err)
    }
}

function errorHandler (err, req, res, next) {
    res.status(500)
    res.render('error', { error: err })
}*/
