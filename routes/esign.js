/**
 * Created by gururaj on 4/17/17.
 */
var express   = require('express');
var router    = express.Router();
var mongojs   = require('mongojs');
var http      = require('http'),
    url       = require('url');

var fs        = require('fs');
var path      = require('path');
var mime      = require('mime');


//var db = mongojs('mongodb://brad:brad@ds047666.mlab.com:47666/mytasklist_brad', ['tasks']);
//var db = mongojs('mongodb://localhost:27017/signdesk', ['sign_desk_users']);

// Get All Tasks
router.get('/requestOTP/:aadhaar', function(req, res, next){

try{

    console.log('req.params.aadhaar',req.params.aadhaar);

    var aadharNo= req.params.aadhaar.toString();   //914156057818
    var today   = new Date();
  /*  var options = {
        host: 'https://signdesk.com',
        port: 8080,
        path: '/esignService/api/staging/otpCall',
        headers:{"X-Parse-Application-Id":"rakesh","X-Parse-REST-API-Key":"q1w2e3r4","Content-Type":"application/json"},
        json: true,
        method: 'POST'
    };*/

    var opts = url.parse('http://signdesk.com:8080/esignApi/api/staging/otpCall');

    opts.headers = {"X-Parse-Application-Id":"rakesh","X-Parse-REST-API-Key":"q1w2e3r4","Content-Type":"application/json"};
    opts.json=true;
    opts.method= "POST";


    var httprequest= http.request(opts, function(signdeskResponse) {

        var finalResult = '';

        //another chunk of data has been recieved, so append it to `str`
        signdeskResponse.on('data', function (chunk) {
            finalResult += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        signdeskResponse.on('end', function () {
            console.log(finalResult);


           var jsonObject = JSON.parse(finalResult);
            if(jsonObject.ErrorCode==='NA'){
                res.status(200);
                res.json(finalResult);
            } else{
                res.status(400);
                res.json(finalResult);
            }
        });

    });

    var data = {"AadhaarNo":aadharNo,"Reference_Id":Date.now().toString(),"TimeStamp":today.toISOString().toString()};
    httprequest.end(JSON.stringify(data));

}catch(e){
    console.log(e)
}

});



router.get('/download/:esignedFile', function(req, res){
    var file = 'signed/'+ req.params.esignedFile;
    var filename = path.basename(file);
    var mimetype = mime.lookup(file);
    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(file);
    filestream.pipe(res);
});


router.get('/getSigned/:aadhaar/:otp/:transactionId/:referenceId/:filename', function(req, res){
console.log(req.params);
    var exec = require('child_process').exec,child;
     child = exec('java -jar dist/eSign.jar uploads/'+req.params.filename+' signed/signed_'+req.params.filename+' '+req.params.aadhaar+' Y '+req.params.otp+' '+req.params.transactionId+' '+req.params.referenceId, function(error,stdout,stderr){
         if(stdout){
             res.status(200);
             res.json(stdout);
         }

         if(stderr){
             res.status(400);
             res.send(stderr);
         }

         if(error){
             res.status(400);
             res.send(error);
         }



     });

});



module.exports = router;